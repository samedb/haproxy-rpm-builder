#!/bin/bash

haproxyVersion="1.8.9"
haRelease="1"


haproxyUrl="http://www.haproxy.org/download/${haproxyVersion:0:3}/src/haproxy-${haproxyVersion}.tar.gz"

which wget > /dev/null
if [ $? -ne 0 ]; then
  echo "Aborting. Cannot continue without wget."
  exit 1
fi

which rpmbuild > /dev/null
if [ $? -ne 0 ]; then
  echo "Aborting. Cannot continue without rpmbuild. Please install the rpmdevtools package."
  exit 1
fi

# Let's get down to business
TOPDIR=$(pwd)

function buildit {
    echo "Building unsigned RPM..."
    rpmbuild --define "_topdir ${TOPDIR}/rpmbuild" \
             --define "version ${haproxyVersion}" \
             --define "release ${haRelease}" \
             --define "_builddir %{_topdir}/BUILD" \
             --define "_buildroot %{_topdir}/BUILDROOT" \
             --define "_rpmdir %{_topdir}/RPMS" \
             --define "_srcrpmdir %{_topdir}/SRPMS" -ba $1


  if [ $? -ne 0 ]; then
    echo "Build failed. Exiting..."
    exit 1
  fi
}

LIBS=(
  'pcre-devel'
  'openssl-devel'
  'zlib-devel'
)
NEEDSLIBS=''
for l in ${LIBS[@]}; do
  echo "Checking for library: ${l}"
  rpm -qa | egrep $l 2>&1 >/dev/null
  if [ $? -ne 0 ]; then
    NEEDSLIBS="${l},${NEEDSLIBS}"
  fi
done
if [ "${NEEDSLIBS}" != '' ]; then
  echo "Need libraries: ${NEEDSLIBS:0:(${#NEEDSLIBS} - 1)}"
  exit 1
fi

if [ -e rpmbuild ]; then
  rm -rf rpmbuild/* 2>&1 > /dev/null
fi

echo "Creating RPM build path structure..."
mkdir -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS,tmp}

echo "Building HAProxy RPM ..."
cp ${TOPDIR}/files/haproxy.spec ${TOPDIR}/rpmbuild/SPECS/


cd ${TOPDIR}/rpmbuild/SOURCES/
wget ${haproxyUrl}
cp ${TOPDIR}/files/* .

cd ${TOPDIR}/rpmbuild/
buildit "SPECS/haproxy.spec"
