This builder is a combination of following repositories plus my contributions:

https://github.com/DBezemer/rpm-haproxy

https://github.com/jsumners/haproxy-rhel7

Thanks to original authors for their excellent work.

## Usage

You can use Docker to build the RPM: (Defaults to Amazon Linux Image)

1. `docker build -t haproxybuilder .`
2. `docker run -v /tmp:/output -it --rm haproxybuilder`
3. Look in your `/tmp` for the RPMs

Note: you may want to do a `docker prune` afterward to clean up.
